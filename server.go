package main

import (
	"fmt"
	"log"
	"net/http"
)

const indexcontext = `
<html>
<title>A Basicly Simple Web Server</title>
<head>
<meta charset="UTF-8">
<style>
h1 {
  display: inline;
}

h1:hover {
  background-color: skyblue;
}
</style>
</head>
<body>
<center>
<h1>Hola!</h1>
<h2>Como Estas?</h2>
</center>
</body>
</html>
`

func hihandler(rw http.ResponseWriter, req *http.Request) {
	if req.Method != "GET" {
		http.Error(rw, "Method is not supported.", http.StatusNotFound)
		return
	}

	fmt.Fprintf(rw, indexcontext)
}

func formhandler(rw http.ResponseWriter, req *http.Request) {
	if err := req.ParseForm(); err != nil {
		fmt.Fprintf(rw, "Parseform() err: %v", err)
		return		
	}

	fmt.Fprintf(rw, "Post request successful\n")
	
	name   := req.FormValue("name")
	addres := req.FormValue("address")
	fmt.Fprintf(rw, "Name    = %s\n", name)
	fmt.Fprintf(rw, "Address = %s\n", addres)
}

func main() {
	fileServer := http.FileServer(http.Dir("./static"))
	http.Handle("/", fileServer)

	http.HandleFunc("/hi", hihandler)
	http.HandleFunc("/form", formhandler)

	fmt.Println("Starting Server at http://localhost:8888 !")

	if err := http.ListenAndServe(":8888", nil); err != nil {
		log.Fatal(err)
	}
}
